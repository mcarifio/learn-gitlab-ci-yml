#!/usr/bin/env bash

_here=$(cd ${0%/*}; pwd)
source ${_here}/${0##*/}.env

main() {
    local _id=$(curl -s -X POST --fail -F token=${PIPELINE_TOKEN:?'expecting a pipeline token'} -F ref=${1:-main} https://gitlab.com/api/v4/projects/${GITLAB_PROJECT:?'expecting a project id'}/trigger/pipeline | jq -r .id)
    local _suffix=$(git remote get-url origin)
    local _url="https://gitlab.com/${_suffix#*:}/-/pipelines/${_id}"
    > /dev/stdout echo "opening ${_url}"
    xdg-open "${_url}"
}

main "$@"

